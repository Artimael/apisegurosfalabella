package com.example.test.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.test.entitys.Venta;

public interface VentasDAO extends JpaRepository<Venta, Long> {

}
