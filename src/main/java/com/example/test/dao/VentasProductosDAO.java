package com.example.test.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.test.entitys.VentaProductos;

public interface VentasProductosDAO extends JpaRepository<VentaProductos, Long> {

}
