package com.example.test.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.test.dao.ProductosDAO;
import com.example.test.dao.VentasDAO;
import com.example.test.dao.VentasProductosDAO;
import com.example.test.entitys.Productos;
import com.example.test.entitys.VentaProductos;
import com.example.test.util.Constantes;

@RestController
@RequestMapping("/EvaluarProductos")
public class EvaluarProductosREST {
	@Autowired
	private ProductosDAO productosDAO;

	@Autowired
	private VentasDAO ventaDAO;

	@Autowired
	private VentasProductosDAO ventaProductosDAO;

	@RequestMapping(value = "{dias}")
	public String evaluarProductos(@PathVariable("dias") int dias) {
		String salida = "";
		List<Productos> listaProductosVendidos = new ArrayList<Productos>();
		List<VentaProductos> listaVentas = ventaProductosDAO.findAll();
		for (VentaProductos venta : listaVentas) {
			Optional<Productos> productoTemporal = productosDAO.findById(venta.getPk_id_producto());
			productoTemporal.get().setPrice(venta.getPrecioProductoVenta());
			listaProductosVendidos.add(productoTemporal.get());
		}

		for (Productos producto : listaProductosVendidos) {
			salida = salida + reglaDescontarProductosDias(dias, producto);
			salida = salida + "##################################### \n";
		}

		return salida;

	}

	private String reglaDescontarProductosDias(int dias, Productos producto) {

		String salida = "";
		for (int i = 0; i < dias; i++) {

			int sellIn = producto.getSellIn();
			int precioActual = producto.getPrice();
			salida = salida + "------DIA " + i + " PRODUCTO:" + producto.getNombre() + "-----\n";
			salida = salida + " Nombre,SellIn, Price\n";
			switch (producto.getNombre()) {
			case Constantes.NOMBRE_PRODUCTO_FULL_COBERTURA:
				// el producto "Full cobertura" incrementa su precio a medida que pasa el
				// tiempo.
				precioActual = precioActual + 1;
				precioActual = validaPrecio(precioActual);
				break;
			case Constantes.NOMBRE_PRODUCTO_FULL_COBERTURA_SUPER_DUPER:
				// El precio se incrementa en 2 cuando quedan 10 dias o menos y se incrementa en
				// 3, cuando quedan 5 dias o menos.
				if (sellIn <= 0) {
					precioActual = 0;
				} else if (sellIn <= 5) {
					precioActual = precioActual + 3;
				} else if (sellIn <= 10) {
					precioActual = precioActual + 2;
				} else {
					precioActual = precioActual + 1;
				}
				precioActual = validaPrecio(precioActual);
				break;
			case Constantes.NOMBRE_PRODUCTO_MEGA_COBERTURA:
				// el producto "Mega cobertura", nunca vence para vender y nunca disminuye su
				// precio.
				precioActual = 180;
				sellIn = sellIn + 1;// Para que se mantenga
				break;
			case Constantes.NOMBRE_PRODUCTO_SUPER_AVANCE:
				precioActual = precioActual - 2;
				break;
			default:
				if (sellIn >= 0) {
					precioActual = precioActual - 1;
				} else {
					precioActual = precioActual - 2;
				}
				precioActual = validaPrecio(precioActual);
				break;
			}
			producto.setPrice(precioActual);
			producto.setSellIn(sellIn - 1);
			salida = salida + producto.getNombre() + " ," + producto.getSellIn() + " ," + producto.getPrice() + "\n";
		}

		return salida;
	}

	private int validaPrecio(int precio) {
		if (precio >= Constantes.PRECIO_MAXIMO) {
			return Constantes.PRECIO_MAXIMO;
		}

		if (precio <= Constantes.PRECIO_MINIMO) {
			return Constantes.PRECIO_MINIMO;
		}
		return precio;
	}

}
