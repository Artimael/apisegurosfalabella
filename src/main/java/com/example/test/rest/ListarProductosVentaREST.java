package com.example.test.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.test.dao.ProductosDAO;
import com.example.test.dao.VentasDAO;
import com.example.test.dao.VentasProductosDAO;
import com.example.test.entitys.Productos;
import com.example.test.entitys.VentaProductos;

@RestController
@RequestMapping("/ListarProductosVenta")
public class ListarProductosVentaREST {
	@Autowired
	private ProductosDAO productosDAO;

	@Autowired
	private VentasProductosDAO ventaProductosDAO;

	@PostMapping
	public String listarProductosVenta() {
		List<Productos> listaProductosVendidos = new ArrayList<Productos>();
		List<VentaProductos> listaVentas = ventaProductosDAO.findAll();
		for (VentaProductos venta : listaVentas) {
			Optional<Productos> productoTemporal = productosDAO.findById(venta.getPk_id_producto());
			listaProductosVendidos.add(productoTemporal.get());
		}
		String salida = "";
		for (Productos producto : listaProductosVendidos) {
			salida = salida + producto.getNombre() + " \n";
		}

		return salida;
	}
}
