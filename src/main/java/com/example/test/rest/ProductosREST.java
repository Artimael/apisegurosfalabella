package com.example.test.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.test.dao.ProductosDAO;
import com.example.test.entitys.Productos;
import com.example.test.util.Constantes;

import ch.qos.logback.classic.pattern.Util;

@RestController
@RequestMapping("/productos")
public class ProductosREST {

	@Autowired
	private ProductosDAO productosDAO;

	@GetMapping
	public ResponseEntity<List<Productos>> getProductos() {
		List<Productos> listaProductos = productosDAO.findAll();
		return ResponseEntity.ok(listaProductos);
	}

	@RequestMapping(value = "{id}") // productos/id
	public ResponseEntity<Productos> getProducto(@PathVariable("id") Long id) {
		Optional<Productos> optionalProducto = productosDAO.findById(id);
		if (optionalProducto.isPresent()) {
			return ResponseEntity.ok(optionalProducto.get());
		} else {
			return ResponseEntity.noContent().build();
		}

	}

	@PostMapping
	public String crearProducto(@RequestBody Productos producto) {

		if (producto.getSellIn() == null) {
			return "Error Producto con Sellin vacio";
		}

		if (producto.getPrice() == null) {
			return "Error Producto con Price vacio";
		}

		if (producto.getPrice() < 0) {
			return "Error Producto con Price negativo";

		}

		if (producto.getPrice() > Constantes.PRECIO_MAXIMO) {
			return "Error Producto con Price mayor al Maximo";
		}
		Date fechaHoy = new Date();
		producto.setFechaActualizacion(convertirFecha(fechaHoy));
		productosDAO.save(producto);
		return "Producto ingresado con exito";
	}

	@DeleteMapping(value = "{id}")
	public String borrarProductos(@PathVariable("id") Long id) {
		productosDAO.deleteById(id);
		return "Producto borrado";
	}

	@PutMapping
	public String actualizarProducto(@RequestBody Productos producto) {
		Optional<Productos> optionalProducto = productosDAO.findById(producto.getId());
		if (optionalProducto.isPresent()) {
			Productos actualizadoProducto = optionalProducto.get();
			actualizadoProducto.setNombre(producto.getNombre());
			productosDAO.save(actualizadoProducto);
			return "Producto Actualizado";
		} else {

			return "Producto no encontrado";
		}
	}
	
	private static java.sql.Date convertirFecha(java.util.Date uDate) {
		java.sql.Date sDate = new java.sql.Date(uDate.getTime());
		return sDate;
	}

}
