package com.example.test.rest;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.test.dao.ProductosDAO;
import com.example.test.dao.VentasDAO;
import com.example.test.dao.VentasProductosDAO;
import com.example.test.entitys.Productos;
import com.example.test.entitys.Venta;
import com.example.test.entitys.VentaProductos;

@RestController
@RequestMapping("/ventaProductos")
public class VentaREST {

	@Autowired
	private ProductosDAO productosDAO;

	@Autowired
	private VentasDAO ventaDAO;

	@Autowired
	private VentasProductosDAO ventaProductosDAO;

	@PostMapping
	public ResponseEntity<Venta> venderProductos(@RequestBody Productos producto) {

		Optional<Productos> optionalProducto = productosDAO.findById(producto.getId());
		if (optionalProducto.isPresent()) {
			Productos productoVenta = optionalProducto.get();
			Venta venta = new Venta();
			Date fechaHoy = new Date();
			venta.setFecha(convertirFecha(fechaHoy));
			ventaDAO.save(venta);
			Venta ultimaVenta = obtenerUltimaVenta(ventaDAO.findAll());
			VentaProductos ventaProductos = new VentaProductos();
			ventaProductos.setPk_id_producto(productoVenta.getId());
			ventaProductos.setPk_id_venta(ultimaVenta.getId());
			ventaProductosDAO.save(ventaProductos);

			return ResponseEntity.ok(venta);
		} else {

			return ResponseEntity.notFound().build();
		}
	}

	private static java.sql.Date convertirFecha(java.util.Date uDate) {
		java.sql.Date sDate = new java.sql.Date(uDate.getTime());
		return sDate;
	}

	private Venta obtenerUltimaVenta(List<Venta> listaVenta) {
		Venta ultimaVenta = new Venta();

		ultimaVenta = listaVenta.get(listaVenta.size() - 1);

		return ultimaVenta;

	}

}
