package com.example.test.util;

public class Constantes {

	public static final String NOMBRE_PRODUCTO_SUPER_AVANCE = "Super Avance";
	public static final String NOMBRE_PRODUCTO_FULL_COBERTURA = "Full Cobertura";
	public static final String NOMBRE_PRODUCTO_MEGA_COBERTURA = "Mega Cobertura";
	public static final String NOMBRE_PRODUCTO_FULL_COBERTURA_SUPER_DUPER = "Full Cobertura Super Duper";
	public static final int PRECIO_MAXIMO = 100;
	public static final int PRECIO_MINIMO = 0;
}
