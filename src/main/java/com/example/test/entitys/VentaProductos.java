package com.example.test.entitys;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "venta_productos")
public class VentaProductos {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "pk_id_venta")
	private Long pk_id_venta;

	@Column(name = "pk_id_producto")
	private Long pk_id_producto;

	@Column(name = "precio_producto_venta")
	private int precioProductoVenta;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPk_id_venta() {
		return pk_id_venta;
	}

	public void setPk_id_venta(Long pk_id_venta) {
		this.pk_id_venta = pk_id_venta;
	}

	public Long getPk_id_producto() {
		return pk_id_producto;
	}

	public void setPk_id_producto(Long pk_id_producto) {
		this.pk_id_producto = pk_id_producto;
	}

	public int getPrecioProductoVenta() {
		return precioProductoVenta;
	}

	public void setPrecioProductoVenta(int precioProductoVenta) {
		this.precioProductoVenta = precioProductoVenta;
	}

}
