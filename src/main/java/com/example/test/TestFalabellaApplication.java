package com.example.test;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import com.example.test.dao.ProductosDAO;
import com.example.test.entitys.Productos;
import com.example.test.util.Constantes;

@SpringBootApplication
public class TestFalabellaApplication {
	@Autowired
	private ProductosDAO productosDAO;

	public static void main(String[] args) {
		SpringApplication.run(TestFalabellaApplication.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void actualizarProductosDiaNuevo() {
		List<Productos> listaProductos = productosDAO.findAll();
		for (Productos producto : listaProductos) {
			Date fechaActualizacion = producto.getFechaActualizacion();
			Date fechaActual = new Date();
			if (!esMismoDia(fechaActualizacion, fechaActual)) {
				producto = reglaDescontarProductos(producto);
				producto.setFechaActualizacion(convertirFecha(fechaActual));
				productosDAO.save(producto);
			}
		}
	}

	private Productos reglaDescontarProductos(Productos producto) {

		int sellIn = producto.getSellIn();
		int precioActual = producto.getPrice();
		switch (producto.getNombre()) {
		case Constantes.NOMBRE_PRODUCTO_FULL_COBERTURA:
			// el producto "Full cobertura" incrementa su precio a medida que pasa el
			// tiempo.
			precioActual = precioActual + 1;
			precioActual = validaPrecio(precioActual);
			break;
		case Constantes.NOMBRE_PRODUCTO_FULL_COBERTURA_SUPER_DUPER:
			// El precio se incrementa en 2 cuando quedan 10 dias o menos y se incrementa en
			// 3, cuando quedan 5 dias o menos.
			if (sellIn <= 0) {
				precioActual = 0;
			} else if (sellIn <= 5) {
				precioActual = precioActual + 3;
			} else if (sellIn <= 10) {
				precioActual = precioActual + 2;
			} else {
				precioActual = precioActual + 1;
			}
			precioActual = validaPrecio(precioActual);
			break;
		case Constantes.NOMBRE_PRODUCTO_MEGA_COBERTURA:
			// el producto "Mega cobertura", nunca vence para vender y nunca disminuye su
			// precio.
			precioActual = 180;
			sellIn = sellIn + 1;// Para que se mantenga
			break;
		case Constantes.NOMBRE_PRODUCTO_SUPER_AVANCE:
			precioActual = precioActual - 2;
			break;
		default:
			if (sellIn >= 0) {
				precioActual = precioActual - 1;
			} else {
				precioActual = precioActual - 2;
			}
			precioActual = validaPrecio(precioActual);
			break;
		}
		producto.setPrice(precioActual);
		producto.setSellIn(sellIn - 1);

		return producto;
	}

	private int validaPrecio(int precio) {
		if (precio >= Constantes.PRECIO_MAXIMO) {
			return Constantes.PRECIO_MAXIMO;
		}

		if (precio <= Constantes.PRECIO_MINIMO) {
			return Constantes.PRECIO_MINIMO;
		}
		return precio;
	}

	private boolean esMismoDia(Date fechaActualizacion, Date fechaActual) {
		SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
		return fmt.format(fechaActualizacion).equals(fmt.format(fechaActual));

	}

	private static java.sql.Date convertirFecha(java.util.Date uDate) {
		java.sql.Date sDate = new java.sql.Date(uDate.getTime());
		return sDate;
	}
}
