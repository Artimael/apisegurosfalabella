*Proyecto Spring para Test de Desarrollador de Software Falabella**

Este proyecto fue creado  para mostrar un desarrollo completo de  Spring.


---

## Instalacion

Para instalar este proyecto es necesario lo siguiente:

1. Clonar repositorio.
2. Importar la BD. Si fuera necesario modifucar datos de en  **application.properties**..
3. Importar proecto postman con consultas listas.
4. Probar proyecto.
.

---

## Tecnologia Usada


1. Spring Boot.
2. JAVA SE 11.
3. MYSQL.
4. POSTMAN.

